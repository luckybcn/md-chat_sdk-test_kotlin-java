package com.meetingdoctors.myapplication

import android.app.Application
import android.content.Context
import com.meetingdoctors.chat.MeetingDoctorsClient
import com.meetingdoctors.chat.views.ProfessionalList
import com.meetingdoctors.mdsecure.OnResetDataListener

class TestKotlin: Application() {

    override fun onCreate() {
        MeetingDoctorsClient.newInstance(Application(),
        "",
        true,
        false,
        "")

        MeetingDoctorsClient.instance?.authenticate("", object: MeetingDoctorsClient.AuthenticationListener {
            override fun onAuthenticated() {
                TODO("Not yet implemented")
            }

            override fun onAuthenticationError() {
                TODO("Not yet implemented")
            }

        })

        MeetingDoctorsClient.instance?.deauthenticate(object: OnResetDataListener {
            override fun dataResetError(exception: Exception?) {
                super.dataResetError(exception)
            }

            override fun dataResetSuccessFul() {
                super.dataResetSuccessFul()
            }
        })

        MeetingDoctorsClient.instance?.setVideoCallRequestListener(object: MeetingDoctorsClient.OnVideoCallRequest {
            override fun perform1to1VideoCall(professionalHash: String?, doctorName: String?, context: Context?, videoCallRequestDoneListener: MeetingDoctorsClient.OnVideoCall1to1RequestDone?) {
                TODO("Not yet implemented")
            }

            override fun performCancelVideoCall(context: Context?, message: String?, videoCallRequestDoneListener: MeetingDoctorsClient.OnVideoCall1to1RequestDone?, doctorName: String?) {
                TODO("Not yet implemented")
            }

            override fun hasProfessionalAssignedVideoCall(professionalHash: String?): Boolean {
                TODO("Not yet implemented")
            }

        })

        MeetingDoctorsClient.instance?.deauthenticate()

        MeetingDoctorsClient.instance?.onFirebaseMessageReceived(null, null)

        MeetingDoctorsClient.instance?.openMedicalHistory(this)

        //ALTERNATIVE INSTANCE
        MeetingDoctorsClient.Companion.instance?.openMedicalHistory(this)

        MeetingDoctorsClient.instance?.authenticate("",
        object: MeetingDoctorsClient.AuthenticationListener {
            override fun onAuthenticated() {
                TODO("Not yet implemented")
            }

            override fun onAuthenticationError() {
                TODO("Not yet implemented")
            }
        })

        val professionalList =ProfessionalList(this)

        professionalList.setProfessionalListListener(object: ProfessionalList.ProfessionalListListener {
            override fun onProfessionalClick(
                p0: Long,
                p1: String?,
                p2: Boolean,
                p3: Boolean
            ): Boolean {
                TODO("Not yet implemented")
            }

            override fun onListLoaded() {
                TODO("Not yet implemented")
            }

            override fun onUnreadMessageCountChange(p0: Long) {
                TODO("Not yet implemented")
            }

        })

        professionalList.setDisabledProfessionalColor(R.color.colorAccent)

        val prof = ProfessionalList(this)

        prof.setDisabledProfessionalColor(R.color.colorAccent)
        prof.setDividerView(null)
    }
}