package com.meetingdoctors.myapplication;

import android.app.Application;
import android.content.Context;

import com.meetingdoctors.chat.MeetingDoctorsClient;
import com.meetingdoctors.chat.views.ProfessionalList;
import com.meetingdoctors.mdsecure.OnResetDataListener;

import org.jetbrains.annotations.Nullable;

public class TestJava extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        MeetingDoctorsClient.newInstance(this,
                "",
                true,
                false,
                "");

        MeetingDoctorsClient.instance.setCollegiateNumbersVisibility(true);

        MeetingDoctorsClient.instance.authenticate("", new MeetingDoctorsClient.AuthenticationListener() {
            @Override
            public void onAuthenticated() {

            }

            @Override
            public void onAuthenticationError() {

            }
        });

        MeetingDoctorsClient.instance.deauthenticate(new OnResetDataListener() {
            @Override
            public void dataResetSuccessFul() {

            }

            @Override
            public void dataResetError(@Nullable Exception e) {

            }
        });

        MeetingDoctorsClient.instance.setVideoCallRequestListener(new MeetingDoctorsClient.OnVideoCallRequest() {
            @Override
            public void perform1to1VideoCall(@Nullable String s, @Nullable String s1, @Nullable Context context, @Nullable MeetingDoctorsClient.OnVideoCall1to1RequestDone onVideoCall1to1RequestDone) {

            }

            @Override
            public void performCancelVideoCall(@Nullable Context context, @Nullable String s, @Nullable MeetingDoctorsClient.OnVideoCall1to1RequestDone onVideoCall1to1RequestDone, @Nullable String s1) {

            }

            @Override
            public boolean hasProfessionalAssignedVideoCall(@Nullable String s) {
                return false;
            }
        });

        MeetingDoctorsClient.instance.openMedicalHistory(this);

        //ALTERNATIVE INSTANCE
        MeetingDoctorsClient.Companion.getInstance().openMedicalHistory(this);

        MeetingDoctorsClient.instance.onFirebaseMessageReceived(null, null);

         ProfessionalList prof = new ProfessionalList(this);

         prof.setDisabledProfessionalColor(R.color.colorAccent);
         prof.setDividerView(null);
    }
}
